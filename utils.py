import time


def time_it(foo):
    is_print = False

    def decorator(*args, **kwargs):
        last_time = time.time()
        foo(*args, **kwargs)
        if is_print:
            print(f"time for {foo.__name__} is: {1 / (time.time() - last_time)}")
    return decorator
