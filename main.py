import cv2
from Vision import Vision
from Swiper import Swiper
from Stablizer import Stablizer
from Assets import Assets
from Picker import Picker
import time
from FrameValidator import FrameValidator
import numpy as np


def main():
    game(max_swipes=5200)


def game(max_swipes):
    shoot_every = 0.75
    is_swiping = True
    swiper = Swiper(shoot_every, is_enabled=is_swiping, limit=max_swipes)
    assets = Assets()

    vision_milk_left = Vision(assets.milk_left)
    vision_milk_right = Vision(assets.milk_right)
    picker_milk_left = Picker(vision_milk_left)
    picker_milk_right = Picker(vision_milk_right)

    frame_validator = FrameValidator()

    default_swipe_duration = 0.05
    while 'Screen capturing':
        if cv2.waitKey(25) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break

        pos, vel = picker_milk_left.pick()

        img = vision_milk_left.current_frame_color()

        if pos is None or vel is None:
            cv2.imshow("img", img)
            continue

        b, g, r = picker_milk_left.color_at(pos)
        got_milk = validate_milk_color(b, g, r, is_print=False)
        want_to_die = swiper.swipe_limit < swiper.swipes

        impact_pos = swiper.calculate_impact_point(pos[0], pos[1], vel, default_swipe_duration)
        rand = np.random.randint(-4, 4, 2)
        impact_pos = (impact_pos[0] + rand[0], impact_pos[1] + rand[1])

        target_color = (255, 0, 255)
        milk_color = (0, 255, 0)
        bad_color = (0, 0, 255)
        if got_milk \
            and frame_validator.validate2(img, is_enabled=not want_to_die)\
            and swiper.validate_imact_point(impact_pos[0], impact_pos[1], vel,  default_swipe_duration, is_enabled=not want_to_die):
            cv2.circle(img, tuple(impact_pos), 10, target_color, 2)
            cv2.circle(img, tuple(pos), 10, milk_color, 2)
            cv2.imshow("img", img)
            swiper.swipe_to(impact_pos[0], impact_pos[1], vel, duration=default_swipe_duration)
        else:
            cv2.circle(img, tuple(pos), 10, bad_color, 2)
            cv2.circle(img, tuple(impact_pos), 10, target_color, 2)
            cv2.imshow("img", img)


def validate_milk_color(b, g, r, is_print):
    if b is None:
        return False
    if is_print:
        print(b, g, r)
    return b > 200 and g > 200 and r > 200



if __name__ == "__main__":
    main()