import os
import cv2


class Assets:
    def __init__(self):
        self._load_milk_left()
        self._load_milk_right()

    @staticmethod
    def _load_asset(path):
        assert os.path.isfile(path)
        loaded = cv2.imread(path)
        loaded = cv2.cvtColor(loaded, cv2.COLOR_BGR2GRAY)
        return loaded

    def _load_milk_left(self):
        print(os.getcwd())
        # milk_left_path = os.getcwd() + "/assets/milk_left.bmp"
        # milk_left_path = os.getcwd() + "/assets/milk_left2.bmp"
        milk_left_path = os.getcwd() + "/assets/milk.bmp"
        self.milk_left = self._load_asset(milk_left_path)

    def _load_milk_right(self):
        print(os.getcwd())
        milk_left_path = os.getcwd() + "/assets/milk_right.bmp"
        self.milk_right = self._load_asset(milk_left_path)