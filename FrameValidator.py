import numpy as np


class FrameValidator:
    def __init__(self):
        self._brown_color_bgr = (60, 90, 147)
        self._orange_color_bgr = (0, 149, 248)

        self._distance = 5

    def validate(self, frame, is_enabled=True):
        if not is_enabled:
            return True

        b = frame[:, :, 0]
        g = frame[:, :, 1]
        r = frame[:, :, 2]

        brown_mask_b = np.logical_and(self._brown_color_bgr[0] - self._distance < b,
                      b < self._brown_color_bgr[0] + self._distance)
        brown_mask_g = np.logical_and(self._brown_color_bgr[1] - self._distance < g,
                                      g < self._brown_color_bgr[1] + self._distance)
        brown_mask_r = np.logical_and(self._brown_color_bgr[2] - self._distance < r,
                                      r < self._brown_color_bgr[2] + self._distance)
        brown_in_image = np.logical_and(brown_mask_b, brown_mask_g)
        brown_in_image = np.logical_and(brown_in_image, brown_mask_r)

        orange_mask_b = np.logical_and(self._orange_color_bgr[0] - self._distance < b,
                                      b < self._orange_color_bgr[0] + self._distance)
        orange_mask_g = np.logical_and(self._orange_color_bgr[1] - self._distance < g,
                                      g < self._orange_color_bgr[1] + self._distance)
        orange_mask_r = np.logical_and(self._orange_color_bgr[2] - self._distance < r,
                                      r < self._orange_color_bgr[2] + self._distance)
        orange_in_image = np.logical_and(orange_mask_b, orange_mask_g)
        orange_in_image = np.logical_and(orange_in_image, orange_mask_r)

        return np.count_nonzero(brown_in_image) == 0 and np.count_nonzero(orange_in_image) == 0

    def validate2(self, frame, is_enabled=True):
        if not is_enabled:
            return True

        b = frame[420:480, :, 0]
        g = frame[420:480, :, 1]
        r = frame[420:480, :, 2]

        brown_exists_b = np.abs(b - self._brown_color_bgr[0]) < self._distance
        brown_exists_g = np.abs(g - self._brown_color_bgr[1]) < self._distance
        brown_exists_r = np.abs(r - self._brown_color_bgr[2]) < self._distance

        brown_exists_bg = np.logical_and(brown_exists_b, brown_exists_g)
        brown_exists_bgr = np.logical_and(brown_exists_bg, brown_exists_r)

        brown_result = brown_exists_bgr.any()


        orange_exists_b = np.abs(b - self._orange_color_bgr[0]) < self._distance
        orange_exists_g = np.abs(g - self._orange_color_bgr[1]) < self._distance
        orange_exists_r = np.abs(r - self._orange_color_bgr[2]) < self._distance

        orange_exists_bg = np.logical_and(orange_exists_b, orange_exists_g)
        orange_exists_bgr = np.logical_and(orange_exists_bg, orange_exists_r)

        orange_result = orange_exists_bgr.any()

        # print(f"brown: {brown_result}, orange: {orange_result}")
        orange_or_brown = (brown_result or orange_result)
        # print(f"orange_or_brown: {orange_or_brown}")

        return not orange_or_brown