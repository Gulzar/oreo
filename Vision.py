import cv2
import numpy as np
import mss
from utils import time_it
import os
from Assets import Assets
from matplotlib import pyplot as plt


class Vision:
    def __init__(self, template):
        self._show_inputs = True
        self._current_frame_gray = None
        self._current_frame_color = None
        self._roi_height = 715  # 100/715
        self._roi_width = 320
        self._roi_top = 0  # 380/0
        self._roi_left = 0

        self._bottom_right = None
        self._top_left = None

        self._template = template

    def milk_position(self):
        return self._top_left, self._bottom_right

    # @time_it
    def decode_frame(self):
        self._get_frame()
        if self._show_inputs:
            self._show_current_frame()

        self._find_template(self._template)

    @time_it
    def _get_frame(self):
        monitor = {'top': self._roi_top, 'left': self._roi_left, 'width': self._roi_width, 'height': self._roi_height}
        with mss.mss() as sct:
            self._current_frame_color = np.array(sct.grab(monitor))

    def _show_current_frame(self):
        win_name = 'OpenCV/Numpy normal'
        cv2.imshow(win_name, self._current_frame_color)
        cv2.moveWindow(win_name, 1000, 30)

    def _find_template(self, template_img):
        self._current_frame_gray = cv2.cvtColor(self._current_frame_color, cv2.COLOR_BGR2GRAY)
        w, h = template_img.shape

        meth = 'cv2.TM_CCOEFF'
        img = self._current_frame_gray.copy()
        method = eval(meth)

        res = cv2.matchTemplate(img, template_img, method)
        min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

        if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
            self._top_left = min_loc
        else:
            self._top_left = max_loc
        self._bottom_right = (self._top_left[0] + w, self._top_left[1] + h)

    def current_frame(self):
        return self._current_frame_gray

    def current_frame_color(self):
        return self._current_frame_color


