import pyautogui
import time
import numpy as np


class Swiper:
    def __init__(self, shoot_every,  is_enabled=False, limit=5):
        self.swipes = 1
        self.swipe_limit = limit

        self._screen_width = 320
        self._oreo_location_x = self._screen_width // 2
        self._oreo_location_y = 640
        self._is_enabled = is_enabled
        self._shoot_every = shoot_every
        self._last_shot_time = time.time()

        self._delta_center = 20
        self._delta_side = 78

        self._oreo_vel = 5

    def calculate_impact_point(self, current_x, current_y, x_velocity, swipe_duration):
        if x_velocity < 0:
            x_velocity = -10
        elif x_velocity > 0:
            x_velocity = 10

        impact_y = current_y
        impact_x = self._calculate_impact_point_x(current_x, x_velocity)

        return impact_x, impact_y

    def swipe_to(self, x, y, x_velocity, duration=0.2):
        if not self._is_enabled:
            return
        # if self.swipes >= self.swipe_limit:
        #     return
        rand = np.random.randint(-4, 4, 2)
        start_x, start_y = self._oreo_location_x + rand[0], self._oreo_location_y + rand[1]

        print(f"shot #{self.swipes}/{self.swipe_limit}")
        self.swipes += 1
        pyautogui.moveTo(start_x, start_y)

        if x < 0:
            x = 0
        if x > 320:
            x = 320

        pyautogui.dragTo(x, y, duration, button='left')
        pyautogui.mouseUp(x, y, button='left')
        self._last_shot_time = time.time()

    def validate_imact_point(self, x, y, vel, duration, is_enabled=True):
        if not is_enabled:
            return True
        if duration is None:
            return False
        time_left = time.time() - self._last_shot_time
        if time_left < self._shoot_every:
            # print(f"still have to wait for {time_left}")
            return False
        # print(f"impact x: {x} is valid")
        w = self._screen_width

        if vel > 0:
            return w//2+self._delta_center < x < w-self._delta_side
        elif vel < 0:
            return self._delta_side < x < w//2-self._delta_center
        else:
            return False

    def _calculate_impact_point_x(self, start_x, x_velocity):

        time = 15
        target_without_correction = int(start_x + time * x_velocity)

        correctiom = abs(target_without_correction - self._screen_width // 2)
        factor = 0.5
        return int(target_without_correction + correctiom * factor * np.sign(x_velocity))