import cv2
import numpy as np
import mss
from utils import time_it
import os
from Assets import Assets
from matplotlib import pyplot as plt


class Segmentation:
    def __init__(self, background):
        self._show_inputs = True
        self._roi_height = 715  # 100/715
        self._roi_width = 280
        self._roi_top = 0  # 380/0
        self._roi_left = 0

        self._bottom_right = None
        self._top_left = None

    def decode_frame(self):
        self._get_frame()
        if self._show_inputs:
            self._show_current_frame()

        self._find_regions()

    @time_it
    def _get_frame(self):
        monitor = {'top': self._roi_top, 'left': self._roi_left, 'width': self._roi_width, 'height': self._roi_height}
        with mss.mss() as sct:
            self._current_frame_color = np.array(sct.grab(monitor))

    def _show_current_frame(self):
        win_name = 'OpenCV/Numpy normal'
        cv2.imshow(win_name, self._current_frame_color)
        cv2.moveWindow(win_name, 1000, 30)

    def _find_regions(self):
        raise NotImplemented