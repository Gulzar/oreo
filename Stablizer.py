import numpy as np


class Stablizer:
    def __init__(self):
        self._history = []
        self._max_history_length = 3
        self._possible_min_y = 400
        self._possible_max_y = 480

        self._velocity_change_threshold = 6
        self._velocity = None

    def next(self, bbox):
        top_left, bottom_right = bbox[0], bbox[1]
        self._history.append((top_left, bottom_right))
        if len(self._history) > self._max_history_length:
            self._history.pop(0)
        self._calculate_velocity()

    def _calculate_center(self, top_left, bottom_right):
        return (top_left[0] + bottom_right[0]) // 2, (top_left[1] + bottom_right[1]) // 2

    def _validate_bbox(self, center):
        history_valid = self._validate_history()
        position_valid = self._validate_position(center)
        movement_valid = self._validate_movement()
        return history_valid and position_valid and movement_valid

    def _validate_history(self):
        return len(self._history) == self._max_history_length

    def _validate_position(self, center):
        center_y = center[1]
        return self._possible_min_y <= center_y <= self._possible_max_y

    def _validate_movement(self):
        return self._velocity is not None

    def valid_bbox(self):
        np_history = np.array(self._history)
        history_mean = np.mean(np_history, axis=0, dtype=int)
        top_left = history_mean[0]
        bottom_right = history_mean[1]
        center = ((top_left[0] + bottom_right[0]) // 2, (top_left[1] + bottom_right[1]) // 2)
        if not self._validate_bbox(center):
            return None, None, None

        return top_left, bottom_right, center

    def _calculate_velocity(self):
        if not self._validate_history():
            return False

        np_history = np.array(self._history)
        top_lefts = np_history[:, 0, :]
        bottom_rights = np_history[:, 1, :]
        centers = (top_lefts + bottom_rights) // 2

        velocities = np.diff(centers[:, 0])
        if np.count_nonzero(velocities) < len(velocities):
            return False

        last_velocity = None
        for velocity in velocities:
            if last_velocity is None:
                last_velocity = velocity
            if abs(velocity - last_velocity) > self._velocity_change_threshold:  # velocity changed! can't happen
                self._velocity = None
                return
            last_velocity = velocity
        self._velocity = np.mean(velocities, dtype=int)

    def velocity(self):
        return self._velocity