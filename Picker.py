from Stablizer import Stablizer
import numpy as np


class Picker: #pick point from single image
    def __init__(self, vision_obj):
        self._vision = vision_obj
        self._stabalizer = Stablizer()

        self._radius_around_point = 10

    def pick(self):
        self._vision.decode_frame()
        milk_position = self._vision.milk_position()
        self._stabalizer.next(bbox=milk_position)
        top_left, bottom_right, center = self._stabalizer.valid_bbox()
        x_velocity = self._stabalizer.velocity()

        if center is None or x_velocity is None:
            # if center is None:
            #     print("center is None")
            # if x_velocity is None:
            #     print("vel is None")
            return None, None

        if x_velocity < 0:
            correction_left = -30
            center = center[0] + correction_left, center[1]  # just fix the detected spot hard coded
        else:
            correction_right = -10
            center = center[0] + correction_right, center[1]  # just fix the detected spot hard coded
        return center, x_velocity

    def current_frame_color(self):
        return self._vision.current_frame_color()

    def color_at(self, point):
        if point is None:
            return None, None, None
        half_rad = self._radius_around_point // 2
        img = self.current_frame_color()
        x_start = point[0] - half_rad
        x_end = point[0] + half_rad
        y_start = point[1] - half_rad
        y_end = point[1] + half_rad

        roi_rgb = img[:, :, 0:3]
        roi = roi_rgb[y_start: y_end,
                      x_start: x_end, :]
        blue = roi[0]
        green = roi[1]
        red = roi[2]

        return np.mean(blue), np.mean(green), np.mean(red),